"""
DIGITS data example
===================

Example to apply K-means algorithm on the Digits dataset.

.. contents:: **Contents**
    :local:
    :depth: 1

"""
# Authors: Hamza Cherkaoui <hamza.cherkaoui@inria.fr>
# License: BSD (3-clause)
import os
import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
from kmeans import KMeans

# %%

plotdir = 'plots'
if not os.path.exists(plotdir):
    os.makedirs(plotdir)

# %%
digits = datasets.load_digits()
X = digits.data

# %%
n_clusters = 11
km = KMeans(n_clusters=n_clusters, max_iters=20, metric='l2', verbose=3)
km.fit_predict(X)
labels, means, l_error = km.labels, km.means, km.l_error

# %%
plt.figure("Error evolution", figsize=(4, 4))
plt.plot(l_error, lw=2.)
plt.grid()
plt.title("Accuracy evolution\n(between assignations in iteration)",
          fontsize=12)
filename = os.path.join(plotdir, 'error.png')
plt.savefig(filename, dpi=200)

# %%
nrows, ncols = 10, 20
_, axis = plt.subplots(nrows=nrows, ncols=ncols, sharex=True, sharey=True,
                       figsize=(20, 10))
labels_to_plot = list(range(n_clusters))
for i in range(nrows):
    label = labels_to_plot[i]
    mean_to_plot = means[label].reshape((8, 8))
    axis[i, 0].matshow(mean_to_plot)
    axis[i, 0].set_xticks([])
    axis[i, 0].set_yticks([])
    axis[i, 0].set_title("Mean", fontsize=10)
    for j in range(1, ncols):
        X_cluster = X[labels == label, :]
        x_to_plot = X_cluster[j].reshape((8, 8))
        axis[i, j].matshow(x_to_plot)
        axis[i, j].set_xticks([])
        axis[i, j].set_yticks([])
plt.tight_layout()
filename = os.path.join(plotdir, 'visualization.png')
plt.savefig(filename, dpi=200)