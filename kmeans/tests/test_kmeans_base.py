"""Testing module"""
# Authors: Hamza Cherkaoui <hamza.cherkaoui@inria.fr>
# License: BSD (3-clause)

import pytest
import numpy as np
from kmeans.kmeans_base import similarity_metric


def test_similarity_metric():
    labels = np.array([1, 0, 1, 0])
    previous_labels = np.array([1, 0, 1, 0])
    similarity = similarity_metric(labels, previous_labels)
    assert np.all(similarity < np.finfo(np.float64).eps)

    labels = np.array([1, 0, 1, 0])
    previous_labels = np.array([1, 0, 1, 1])
    similarity = similarity_metric(labels, previous_labels)
    assert np.all(similarity > np.finfo(np.float64).eps)


def test_compute_means():  # TODO faire test
    pass

def test_assign_label():  # TODO faire test
    pass