"""
Main KMeans functionalities
"""
# Authors: Hamza Cherkaoui <hamza.cherkaoui@inria.fr>
# License: BSD (3-clause)
import numpy as np


# TODO enh. docstrings


def check_random_state(seed):
    """Turn seed into a np.random.RandomState instance.

    Parameters
    ----------
    seed : None, int, random-instance, (default=None), random-instance
        or random-seed used to initialize the random-instance

    Return
    ------
    random_instance : random-instance used to initialize the analysis
    """
    if seed is None or seed is np.random:
        return np.random.mtrand._rand
    if isinstance(seed, (int, np.integer)):
        return np.random.RandomState(seed)
    if isinstance(seed, np.random.RandomState):
        return seed
    raise ValueError('{0} cannot be used to seed a numpy.random.RandomState'
                     ' instance'.format(seed))


def compute_means(X, labels):
    """ Compute means for each unique labels.

    Parameters
    ----------

    Return
    ------

    """
    unique_labels = np.unique(labels)

    means = []
    for label in unique_labels:
        means.append(np.mean(X[labels == label], axis=0))

    return np.array(means)


def assign_label(X, means, metric='l2-error'):
    """ Assign the label of the closest mean to each x_i.

    Parameters
    ----------

    Return
    ------

    """
    n_samples = len(X)
    n_means = len(means)

    if hasattr(metric, '__call__'):
        metric_func = metric
    elif metric == 'l2':
        metric_func = lambda x, y: np.linalg.norm(x - y)
    elif metric == 'l2-error':
        metric_func = lambda x, y: np.sum(np.square(x - y))
    elif metric == 'l1':
        metric_func = lambda x, y: np.sum(np.abs(x - y))  # TODO not opt
    else:
        raise ValueError("Metric to compute cluster not understood.")

    # TODO Numba this part at least...
    all_distances = np.empty((n_samples, n_means), dtype=float)
    for i, x_i in enumerate(X):
        for j, mean in enumerate(means):
            all_distances[i, j] = metric_func(x_i, mean)

    return np.argmin(all_distances, axis=1)


def similarity_metric(labels, previous_labels):
    """ Compute the accuracy between labels and previous_labels.

    Parameters
    ----------

    Return
    ------

    """
    scores = (labels != previous_labels).astype(int)
    return np.mean(scores)


def k_means(X, n_clusters=3, max_iters=100, metric='l2-error',
            random_seed=None, eps=1e-6, verbose=1):
    """ Main k-means algorithms.

    Parameters
    ----------

    Return
    ------

    """
    X = np.atleast_2d(X)
    n_samples, _ = X.shape  # TODO need better dim' check

    rng = check_random_state(random_seed)
    labels = rng.randint(0, n_clusters, n_samples)

    if verbose >= 1:
        print(f"[K-Means]Launching K-Means algorithm on {n_samples} samples "
              f"with {n_clusters} clusters")

    previous_labels = labels
    l_error = []
    for n_iter in range(max_iters):

        means = compute_means(X, labels)

        labels = assign_label(X, means, metric=metric)

        # TODO explore other criterion
        l_error.append(similarity_metric(labels, previous_labels))

        if l_error[-1] < eps:

            if verbose >= 1:
                print(f"[K-Means][Converged] final error = {l_error[-1]:.8f}")

            break

        previous_labels = labels

        if verbose >= 2:
            print(f"[K-Means][{n_iter:03d}/{max_iters:03d}] error = "
                  f"{l_error[-1]:.8f}")

    return labels, means, l_error
