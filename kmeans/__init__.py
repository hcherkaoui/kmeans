"""
Simple Python package to provide KMeans algorithm
"""
# Authors: Hamza Cherkaoui <hamza.cherkaoui@inria.fr>
# License: BSD (3-clause)
from sklearn.base import ClusterMixin
from .kmeans_base import k_means


# TODO enh. docstrings


class KMeans(ClusterMixin):
    """K-Means clustering.
    """

    def __init__(self, n_clusters=3, max_iters=100, metric='l2-error',
                 random_seed=None, eps=1e-6, verbose=0):
        """ Init.
        """
        # algorithms hyperparameters
        self.n_clusters = n_clusters
        self.max_iters = max_iters
        self.metric = metric
        self.random_seed = random_seed
        self.eps = eps
        self.verbose = verbose

        # cluster results
        self.labels = None
        self.means = None
        self.l_error = None

    def fit_predict(self, X):
        """ Perform the K-means clustering algorithm on X.
        """
        labels, means, l_error = k_means(X, n_clusters=self.n_clusters,
                                         max_iters=self.max_iters,
                                         metric=self.metric,
                                         random_seed=self.random_seed,
                                         eps=self.eps, verbose=self.verbose)
        self.labels = labels
        self.means = means
        self.l_error = l_error