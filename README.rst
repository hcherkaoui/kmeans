.. -*- mode: rst -*-

|Python36|_

.. |Python36| image:: https://img.shields.io/badge/python-3.6-blue.svg
.. _Python36: https://badge.fury.io/py/scikit-learn


Kmeans
======

Simple implementation of the K-Means algorithm.

Important links
===============

- Official source code repo: https://bitbucket.org/hcherkaoui/kmeans

Dependencies
============

The required dependencies to use the software are:

* Numba >= 0.51.2
* Numpy >= 1.14.0
* Matplotlib >= 2.1.2


License
=======

All material is Free Software: BSD license (3 clause).

Test
============

To run all the tests, run the following command from the hemolearn directory::

    python3 -m pytest


Development
===========

Code
----

GIT
~~~

You can check the latest sources with the command::

    git clone https://bitbucket.org/hcherkaoui/kmeans

